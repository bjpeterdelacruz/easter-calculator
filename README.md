# Easter Calculator

This app was released in 2014. A newer version of this app is currently in development, and its source code can be found in the [api-version-32](https://bitbucket.org/bjpeterdelacruz/easter-calculator/src/api-version-32/) branch.

You can download and install the old version from Google Play [here](https://play.google.com/store/apps/details?id=com.bpd.easter).

**Minimum API version**: 4

**Target API version**: 19
