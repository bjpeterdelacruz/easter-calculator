package com.bpd.easter;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public final class EasterCalendar {

  private static final Format DATE_FORMATTER = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());

  private EasterCalendar() {
  }

  private static Date getEaster(int year) {
    int a = year % 19;
    int b = year / 100;
    int c = year % 100;
    int d = b / 4;
    int e = b % 4;
    int f = (b + 8) / 25;
    int g = (b - f + 1) / 3;
    int h = (19 * a + b - d - g + 15) % 30;
    int i = c / 4;
    int k = c % 4;
    int l = (32 + 2 * e + 2 * i - h - k) % 7;
    int m = (a + 11 * h + 22 * l) / 451;
    int month = (h + l - 7 * m + 114) / 31;
    int p = (h + l - 7 * m + 114) % 31;
    int day = p + 1;

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.MONTH, month - 1);
    calendar.set(Calendar.DAY_OF_MONTH, day);
    calendar.set(Calendar.YEAR, year);
    return calendar.getTime();
  }

  private static Date getNextEaster(int month, int day, int year) {
    int nextYear = year;
    int m = 0, d = 0;
    Calendar calendar;
    do {
      calendar = Calendar.getInstance();
      if (nextYear >= 4099) {
        break;
      }
      calendar.setTime(getEaster(++nextYear));
      m = calendar.get(Calendar.MONTH) + 1;
      d = calendar.get(Calendar.DAY_OF_MONTH);
    }
    while (m != month || d != day);
    if (m != month || d != day || nextYear > 4099) {
      return null;
    }
    return calendar.getTime();
  }

  public static Format getFormatter() {
    return DATE_FORMATTER;
  }

  public static boolean isEasterComingSoon(int year) {
    return isEasterComingSoon(year, Calendar.getInstance());
  }

  public static boolean isEasterComingSoon(int year, Calendar calendar) {
    Date today = calendar.getTime();
    Date easter = getEaster(year);
    return today.before(easter) || DATE_FORMATTER.format(today).equals(DATE_FORMATTER.format(easter));
  }

  public static String getEasterSundayDate(int year) {
    if (year < 1583 || year > 4099) {
      throw new IllegalArgumentException("invalid Gregorian year; must be between 1583 and 4099, inclusive");
    }
    return DATE_FORMATTER.format(getEaster(year));
  }

  public static String getNextEasterSundayDate(int month, int day, int year) {
    if (month < 3 || month > 4) {
      throw new IllegalArgumentException("month must be either 3 or 4");
    }
    if (day == 0 || day > 31) {
      throw new IllegalArgumentException("day must be between 1 and 31, inclusive");
    }
    if (year < 1583 || year > 4099) {
      throw new IllegalArgumentException("invalid Gregorian year; must be between 1583 and 4099, inclusive");
    }
    Date date = getNextEaster(month, day, year);
    return date == null ? "" : DATE_FORMATTER.format(date);
  }

}
