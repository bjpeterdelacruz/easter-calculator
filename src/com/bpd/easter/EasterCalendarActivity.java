package com.bpd.easter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class EasterCalendarActivity extends Activity {

  private static final String DATE_KEY = "dateKey";
  private static final String YEAR_KEY = "yearKey";
  private static final String CLOSEST_DATE_KEY = "closestDateKey";
  private static final String MONTH_KEY = "monthKey";
  private static final String DAY_KEY = "dayKey";
  private static final String IS_CHECKED_KEY = "isChecked";
  private static final String AFTER_YEAR_KEY = "afterYearKey";

  private static final int NUMBER_OF_YEARS = 1;
  private static final int GREGORIAN_START = 1583;
  private static final int GREGORIAN_END = 4099;

  private static final int THIS_YEAR = Calendar.getInstance().get(Calendar.YEAR);

  private TextView dateTv;
  private EditText dateEt;
  private Button yearBtn;

  private TextView closestDateTv, onTv;
  private Spinner monthSpinner, daySpinner;
  private CheckBox afterCheck;
  private EditText afterYearEt;
  private Button nextOccurrenceBtn;

  private Resources res;
  private boolean listenerEnabled = false;

  private static Bitmap portraitImage, landscapeImage;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    res = getResources();

    dateTv = new TextView(this);
    dateTv.setTextSize(25);
    dateTv.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    dateTv.setGravity(Gravity.CENTER);
    dateTv.setText("");

    dateEt = new EditText(this);
    dateEt.setInputType(InputType.TYPE_CLASS_NUMBER);
    dateEt.setHint(R.string.hint);
    dateEt.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f));
    dateEt.setImeOptions(EditorInfo.IME_ACTION_DONE);
    dateEt.addTextChangedListener(new TextWatcher() {

      @Override
      public void afterTextChanged(Editable s) {
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        yearBtn.setEnabled(dateEt.getText().toString().length() > 0);
      }

    });

    yearBtn = new Button(this);
    yearBtn.setEnabled(false);
    yearBtn.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f));
    yearBtn.setText(R.string.get_date);
    yearBtn.setBackgroundDrawable(createGradientDrawable());
    yearBtn.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        int year = Integer.parseInt(dateEt.getText().toString());
        if (year < GREGORIAN_START || year > GREGORIAN_END) {
          dateTv.setText(res.getString(R.string.error_msg));
          dateEt.setText("");
          return;
        }
        dateTv.setText(EasterCalendar.getEasterSundayDate(year));
        dateEt.setText("");
      }

    });

    closestDateTv = new TextView(this);
    closestDateTv.setTextSize(25);
    closestDateTv.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    closestDateTv.setGravity(Gravity.CENTER);
    closestDateTv.setText("");

    List<String> days = new ArrayList<String>();
    for (int i = 1; i <= 31; i++) {
      days.add(String.valueOf(i));
    }
    daySpinner = new Spinner(this);
    daySpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, days));
    daySpinner.setSelection(0);

    List<String> months = Arrays.asList(res.getString(R.string.march), res.getString(R.string.april));
    monthSpinner = new Spinner(this);
    monthSpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, months));
    monthSpinner.setSelection(0);
    monthSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (!listenerEnabled) {
          // Selection is made to spinner when restoring state of all views, which will cause this method to execute
          // after all values are restored. Setting listenerEnabled to false will make this method do nothing on
          // orientation change. Simply reset listenerEnabled to true, and everything will be back to normal.
          listenerEnabled = true;
          return;
        }
        int start = position == 0 ? 22 : 1;
        int end = position == 0 ? 31 : 25;
        List<String> days = new ArrayList<String>();
        for (int i = start; i <= end; i++) {
          days.add(String.valueOf(i));
        }
        daySpinner.setAdapter(new ArrayAdapter<String>(EasterCalendarActivity.this,
            android.R.layout.simple_spinner_item, days));
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {
      }

    });

    nextOccurrenceBtn = new Button(this);
    nextOccurrenceBtn.setText(res.getString(R.string.year));
    nextOccurrenceBtn.setBackgroundDrawable(createGradientDrawable());
    nextOccurrenceBtn.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View v) {
        if (afterCheck.isChecked()) {
          int year = Integer.parseInt(afterYearEt.getText().toString());
          if (year < GREGORIAN_START || year > GREGORIAN_END) {
            closestDateTv.setText(res.getString(R.string.error_msg));
            return;
          }
        }
        closestDateTv.setText("");
        new AsyncTask<Void, Void, String>() {

          @Override
          protected String doInBackground(Void... args) {
            String m = monthSpinner.getSelectedItem().toString();
            int month = res.getString(R.string.march).equals(m) ? 3 : 4;
            int day = Integer.parseInt(daySpinner.getSelectedItem().toString());
            if (afterCheck.isChecked()) {
              int year = Integer.parseInt(afterYearEt.getText().toString());
              return EasterCalendar.getNextEasterSundayDate(month, day, year);
            }
            return EasterCalendar.getNextEasterSundayDate(month, day, THIS_YEAR);
          }

          @Override
          protected void onPostExecute(String result) {
            closestDateTv.setText(result.length() == 0 ? res.getString(R.string.no_date_found) : result);
          }

        }.execute();
      }

    });

    afterYearEt = new EditText(this);
    afterYearEt.setEnabled(false);
    afterYearEt.setInputType(InputType.TYPE_CLASS_NUMBER);
    afterYearEt.setHint(R.string.year_hint);
    afterYearEt.addTextChangedListener(new TextWatcher() {

      @Override
      public void afterTextChanged(Editable s) {
      }

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
        nextOccurrenceBtn.setEnabled(afterYearEt.getText().toString().length() > 0 && afterCheck.isChecked());
      }

    });

    afterCheck = new CheckBox(this);
    afterCheck.setText(res.getString(R.string.after));
    afterCheck.setOnCheckedChangeListener(new OnCheckedChangeListener() {

      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
          afterYearEt.setEnabled(true);
          nextOccurrenceBtn.setEnabled(afterYearEt.getText().toString().length() > 0);
        }
        else {
          afterYearEt.setEnabled(false);
          nextOccurrenceBtn.setEnabled(true);
          if (closestDateTv.getText().toString().equals(res.getString(R.string.error_msg))) {
            closestDateTv.setText("");
          }
        }
      }

    });

    onTv = new TextView(this);
    onTv.setText(" " + res.getString(R.string.on));
    onTv.setTextSize(18);

    listenerEnabled = true;
    setContentView(isLandscape() ? createLandscapeLayout() : createPortraitLayout());
  }

  private View createLandscapeLayout() {
    LinearLayout outer = new LinearLayout(this);
    outer.setOrientation(LinearLayout.HORIZONTAL);
    outer.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    outer.setWeightSum(1);

    LinearLayout vertical = new LinearLayout(this);
    vertical.setOrientation(LinearLayout.VERTICAL);
    vertical.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f));

    vertical.addView(dateTv);

    LinearLayout row1 = new LinearLayout(this);
    row1.setOrientation(LinearLayout.HORIZONTAL);
    row1.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    row1.addView(dateEt);
    row1.addView(yearBtn);

    vertical.addView(row1);

    vertical.addView(closestDateTv);

    if (isTablet(10)) {
      LinearLayout row2 = new LinearLayout(this);
      row2.setOrientation(LinearLayout.HORIZONTAL);
      row2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row2.setGravity(Gravity.CENTER);
      row2.addView(nextOccurrenceBtn);
      row2.addView(onTv);
      row2.addView(monthSpinner);
      row2.addView(daySpinner);
      row2.addView(afterCheck);
      row2.addView(afterYearEt);

      vertical.addView(row2);
    }
    else if (isTablet(7)) {
      LinearLayout row2 = new LinearLayout(this);
      row2.setOrientation(LinearLayout.HORIZONTAL);
      row2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row2.setGravity(Gravity.CENTER);
      row2.addView(nextOccurrenceBtn);
      row2.addView(onTv);
      row2.addView(monthSpinner);
      row2.addView(daySpinner);

      vertical.addView(row2);

      LinearLayout row3 = new LinearLayout(this);
      row3.setOrientation(LinearLayout.HORIZONTAL);
      row3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row3.setGravity(Gravity.CENTER);
      row3.addView(afterCheck);
      row3.addView(afterYearEt);

      vertical.addView(row3);
    }
    else {
      LinearLayout row2 = new LinearLayout(this);
      row2.setOrientation(LinearLayout.HORIZONTAL);
      row2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row2.setGravity(Gravity.CENTER);
      row2.addView(nextOccurrenceBtn);

      vertical.addView(row2);

      LinearLayout row3 = new LinearLayout(this);
      row3.setOrientation(LinearLayout.HORIZONTAL);
      row3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row3.setGravity(Gravity.CENTER);
      row3.addView(onTv);
      row3.addView(monthSpinner);
      row3.addView(daySpinner);

      vertical.addView(row3);

      LinearLayout row4 = new LinearLayout(this);
      row4.setOrientation(LinearLayout.HORIZONTAL);
      row4.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row4.setGravity(Gravity.CENTER);
      row4.addView(afterCheck);
      row4.addView(afterYearEt);

      vertical.addView(row4);
    }

    ImageView iv = new ImageView(this);
    if (isTablet(7)) {
      iv.setImageBitmap(createPortraitImage());
      vertical.addView(iv);
    }

    outer.addView(vertical);

    LinearLayout rightPanel = new LinearLayout(this);
    rightPanel.setLayoutParams(new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.5f));
    rightPanel.setOrientation(LinearLayout.VERTICAL);

    TextView tv = createUpcomingDatesTextView(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    rightPanel.addView(tv);
    iv = new ImageView(this);
    iv.setImageBitmap(createLandscapeImage());
    rightPanel.addView(iv);

    outer.addView(rightPanel);

    return outer;
  }

  private View createPortraitLayout() {
    LinearLayout vertical = new LinearLayout(this);
    vertical.setOrientation(LinearLayout.VERTICAL);
    vertical.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));

    vertical.addView(dateTv);

    LinearLayout row1 = new LinearLayout(this);
    row1.setOrientation(LinearLayout.HORIZONTAL);
    row1.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
    row1.addView(dateEt);
    row1.addView(yearBtn);

    vertical.addView(row1);

    vertical.addView(closestDateTv);

    if (isTablet(7)) {
      LinearLayout row2 = new LinearLayout(this);
      row2.setOrientation(LinearLayout.HORIZONTAL);
      row2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row2.setGravity(Gravity.CENTER);
      row2.addView(nextOccurrenceBtn);
      row2.addView(onTv);
      row2.addView(monthSpinner);
      row2.addView(daySpinner);
      row2.addView(afterCheck);
      row2.addView(afterYearEt);

      vertical.addView(row2);
    }
    else {
      LinearLayout row2 = new LinearLayout(this);
      row2.setOrientation(LinearLayout.HORIZONTAL);
      row2.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row2.setGravity(Gravity.CENTER);
      row2.addView(nextOccurrenceBtn);
      row2.addView(onTv);
      row2.addView(monthSpinner);
      row2.addView(daySpinner);

      vertical.addView(row2);

      LinearLayout row3 = new LinearLayout(this);
      row3.setOrientation(LinearLayout.HORIZONTAL);
      row3.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
      row3.setGravity(Gravity.CENTER);
      row3.addView(afterCheck);
      row3.addView(afterYearEt);

      vertical.addView(row3);
    }

    LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
    vertical.addView(createUpcomingDatesTextView(params));

    ImageView iv = new ImageView(this);
    iv.setImageBitmap(createPortraitImage());
    vertical.addView(iv);

    return vertical;
  }

  private Bitmap createPortraitImage() {
    if (portraitImage != null) {
      return portraitImage;
    }

    int id = res.getIdentifier("easter", "drawable", getPackageName());
    portraitImage = BitmapFactory.decodeStream(res.openRawResource(id));
    return portraitImage;
  }

  private Bitmap createLandscapeImage() {
    if (landscapeImage != null) {
      return landscapeImage;
    }

    int id = res.getIdentifier("risen", "drawable", getPackageName());
    landscapeImage = BitmapFactory.decodeStream(res.openRawResource(id));
    return landscapeImage;
  }

  private TextView createUpcomingDatesTextView(LayoutParams params) {
    TextView upcomingDatesTv = new TextView(this);
    upcomingDatesTv.setTextSize(25);
    upcomingDatesTv.setLayoutParams(params);
    upcomingDatesTv.setGravity(Gravity.CENTER);
    upcomingDatesTv.setMovementMethod(new ScrollingMovementMethod());
    upcomingDatesTv.setText(createUpcomingEasterDates());
    return upcomingDatesTv;
  }

  private String createUpcomingEasterDates() {
    StringBuilder builder = new StringBuilder();
    boolean isTablet = isTablet(7);
    if (isTablet) {
      builder.append("\n");
    }
    if (EasterCalendar.isEasterComingSoon(THIS_YEAR)) {
      String text = String.format("%d and %d", THIS_YEAR, THIS_YEAR + 1);
      builder.append(res.getString(R.string.upcoming).replace("XXX", text));
    }
    else {
      String text = String.format("%d", THIS_YEAR + 1);
      builder.append(res.getString(R.string.upcoming).replace("XXX", text));
    }
    builder.append("\n");
    if (EasterCalendar.isEasterComingSoon(THIS_YEAR)) {
      builder.append(EasterCalendar.getEasterSundayDate(THIS_YEAR));
      builder.append("\n");
    }
    for (int start = THIS_YEAR + 1; start <= THIS_YEAR + NUMBER_OF_YEARS; start++) {
      builder.append(EasterCalendar.getEasterSundayDate(start));
      if (isTablet) {
        builder.append("\n");
      }
    }
    return builder.toString();
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putString(DATE_KEY, dateTv.getText().toString());
    outState.putString(YEAR_KEY, dateEt.getText().toString());
    outState.putString(CLOSEST_DATE_KEY, closestDateTv.getText().toString());
    outState.putString(MONTH_KEY, monthSpinner.getSelectedItem().toString());
    outState.putString(DAY_KEY, daySpinner.getSelectedItem().toString());
    outState.putBoolean(IS_CHECKED_KEY, afterCheck.isChecked());
    outState.putString(AFTER_YEAR_KEY, afterYearEt.getText().toString());
  }

  @Override
  protected void onRestoreInstanceState(Bundle savedInstanceState) {
    if (savedInstanceState != null) {
      if (savedInstanceState.containsKey(DATE_KEY)) {
        dateTv.setText(savedInstanceState.getString(DATE_KEY));
      }
      if (savedInstanceState.containsKey(YEAR_KEY)) {
        dateEt.setText(savedInstanceState.getString(YEAR_KEY));
      }
      if (savedInstanceState.containsKey(CLOSEST_DATE_KEY)) {
        closestDateTv.setText(savedInstanceState.getString(CLOSEST_DATE_KEY));
      }
      if (savedInstanceState.containsKey(MONTH_KEY)) {
        String month = savedInstanceState.getString(MONTH_KEY);
        monthSpinner.setSelection(res.getString(R.string.march).equals(month) ? 0 : 1);
        listenerEnabled = false;
      }
      if (savedInstanceState.containsKey(DAY_KEY)) {
        daySpinner.setSelection(Integer.parseInt(savedInstanceState.getString(DAY_KEY)) - 1);
      }
      boolean isChecked = savedInstanceState.getBoolean(IS_CHECKED_KEY, false);
      if (savedInstanceState.containsKey(IS_CHECKED_KEY)) {
        afterCheck.setChecked(isChecked);
      }
      String afterYear = savedInstanceState.getString(AFTER_YEAR_KEY);
      afterYear = afterYear == null ? "" : afterYear;
      if (savedInstanceState.containsKey(AFTER_YEAR_KEY)) {
        afterYearEt.setText(afterYear);
      }
      nextOccurrenceBtn.setEnabled(!isChecked || (isChecked && afterYear.length() > 0));
    }
    super.onRestoreInstanceState(savedInstanceState);
  }

  private static GradientDrawable createGradientDrawable() {
    GradientDrawable gd = new GradientDrawable();
    gd.setColor(Color.TRANSPARENT);
    gd.setCornerRadius(5);
    gd.setStroke(1, 0xFF000000);
    return gd;
  }

  @Override
  public void onBackPressed() {
    moveTaskToBack(true);
  }

  private boolean isLandscape() {
    return res.getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
  }

  private boolean isTablet(int inch) {
    DisplayMetrics metrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(metrics);
    int widthPixels = metrics.widthPixels;
    int heightPixels = metrics.heightPixels;
    float widthDpi = metrics.xdpi;
    float heightDpi = metrics.ydpi;
    float widthInches = widthPixels / widthDpi;
    float heightInches = heightPixels / heightDpi;
    double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));
    return (int) Math.round(diagonalInches) >= inch;
  }

}
